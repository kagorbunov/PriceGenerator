# Golang final exam
## Transaction price generation service
## Task №1
Postman description :
https://documenter.getpostman.com/view/19090990/VUjSGixJ

Postman description JSON :
https://www.postman.com/collections/81203f5c65946e716205
## Run locally

Please run the services in order.
1) Start task №1
2) Start task №2
3) Start task №3

- Build and run:

```bash
$ git clone https://gitlab.com/kagorbunov/PriceGenerator.git
$ cd PriceGenerator
$ go mod download
$ sudo make run
```

- Test:

```bash
$ sudo make test
```

- Check lint:

```bash
$ sudo make lint-run
```

Server is listening on http://localhost:8090

[GET] http://localhost:8090/currency_pair

[GET] http://localhost:8090/currency_pair/{name}

All configuration parameters in .env