up:
	docker-compose up -d

run: up
	go run main.go

lint:
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin

lint-run: lint
	golangci-lint run

test:
	go test -v ./... -cover