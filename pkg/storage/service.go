package storage

import (
	"context"
	"errors"
	"github.com/go-redis/redis/v9"
	"log"
	"sort"
	"time"
)

type Client struct {
	Redis     *redis.Client
	MaxRecord int
}

type Currency struct {
	CurrencyPair string                `json:"currency_pair"`
	Value        []*TransactionHistory `json:"value"`
}

func NewCurrency(currencyPair string, value []*TransactionHistory) *Currency {
	return &Currency{CurrencyPair: currencyPair, Value: value}
}

type TransactionHistory struct {
	Data  string `json:"data"`
	Price string `json:"price"`
}

func NewTransactionHistory(data string, price string) *TransactionHistory {
	return &TransactionHistory{Data: data, Price: price}
}

func New(redis *redis.Client, maxRecord int) *Client {
	return &Client{
		Redis:     redis,
		MaxRecord: maxRecord,
	}
}

func (c *Client) Set(ctx context.Context, name string, value string, data time.Time) error {
	if value == "" {
		return errors.New("error value is empty")
	}
	err := c.Redis.Set(ctx, name+"|"+data.Format(time.Stamp), value, time.Hour*24)
	if err.Err() != nil {
		log.Fatalf("Error setting in redis name: %v, err: %v", err.Name(), err.Err())
		return err.Err()
	}
	return nil
}

func (c Client) GetValueTransactions(ctx context.Context, name string) (values []string, err error) {
	listTran, err := c.GetListTransactions(ctx, name)
	if err != nil {
		log.Printf("Error with get list transactions: %v", err)
		return
	}
	for _, tran := range listTran {
		value, err := c.Redis.Get(ctx, tran).Result()
		if err != nil {
			log.Fatalf("Error with get value of transaction: %v, err: %v", tran, err)
			//return
		}
		values = append(values, value)
	}
	return
}

func (c Client) GetListTransactions(ctx context.Context, key string) ([]string, error) {
	var cursor uint64 = 0
	var listOfTran, listOfTranIter []string
	var err error
	for {
		listOfTranIter, cursor, err = c.Redis.Scan(ctx, cursor, key+"|*", 0).Result()
		listOfTran = append(listOfTran, listOfTranIter...)
		if err != nil {
			log.Fatalf("Error with scan: %v", err)
			return nil, err
		}
		if cursor == 0 { // no more keys
			break
		}
	}
	sort.Strings(listOfTran)
	if len(listOfTran) == 0 {
		return nil, errors.New("Do not exists records with this key")
	}
	return listOfTran, nil
}

func (c Client) GetOfNumberTransactions(ctx context.Context, name string) (int, error) {
	listTransactions, err := c.GetListTransactions(ctx, name)
	if err != nil {
		log.Printf("Error with get list transactions: %v", err)
		return 0, err
	}
	return len(listTransactions), nil
}

func (c Client) ResetOldRecord(ctx context.Context, name string) error {
	numOfTran, err := c.GetOfNumberTransactions(ctx, name)
	if err != nil {
		return err
	}
	if numOfTran > c.MaxRecord {
		listTransactions, err := c.GetListTransactions(ctx, name)
		if err != nil {
			return err
		}
		resetTransactions := listTransactions[:numOfTran-c.MaxRecord]
		cmd := c.Redis.Del(ctx, resetTransactions...)
		return cmd.Err()
	}
	return nil
}
