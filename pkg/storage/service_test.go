package storage

import (
	"context"
	"github.com/go-redis/redis/v9"
	"reflect"
	"testing"
	"time"
)

func TestClient_Set(t *testing.T) {
	type fields struct {
		Redis     *redis.Client
		MaxRecord int
	}
	type args struct {
		ctx   context.Context
		name  string
		value string
		data  time.Time
	}
	d, _ := time.Parse(time.Stamp, "Aug 15 18:20:00")
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Test 1: Successful",
			fields: fields{
				Redis: redis.NewClient(&redis.Options{
					Addr:     "localhost:6379",
					Password: "", // no password set
					DB:       0,  // use default DB
				}),
				MaxRecord: 100,
			},
			args: args{
				ctx:   context.Background(),
				name:  "RUBJPY",
				value: "75.05",
				data:  d,
			},
			wantErr: false,
		},
		{
			name: "Test 2: Unsuccessful",
			fields: fields{
				Redis: redis.NewClient(&redis.Options{
					Addr:     "localhost:6379",
					Password: "", // no password set
					DB:       0,  // use default DB
				}),
				MaxRecord: 100,
			},
			args: args{
				ctx:   context.Background(),
				name:  "USDRUB",
				value: "",
				data:  time.Now(),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Client{
				Redis:     tt.fields.Redis,
				MaxRecord: tt.fields.MaxRecord,
			}
			if err := c.Set(tt.args.ctx, tt.args.name, tt.args.value, tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("Set() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestClient_GetValueTransactions(t *testing.T) {
	type fields struct {
		Redis     *redis.Client
		MaxRecord int
	}
	type args struct {
		ctx  context.Context
		name string
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		wantValues []string
		wantErr    bool
	}{
		{
			name: "Test 1: Successful",
			fields: fields{
				Redis: redis.NewClient(&redis.Options{
					Addr:     "localhost:6379",
					Password: "", // no password set
					DB:       0,  // use default DB
				}),
				MaxRecord: 100,
			},
			args: args{
				ctx:  context.Background(),
				name: "RUBJPY",
			},
			wantValues: []string{"75.05"},
			wantErr:    false,
		},
		{
			name: "Test 2: Unsuccessful",
			fields: fields{
				Redis: redis.NewClient(&redis.Options{
					Addr:     "localhost:6379",
					Password: "", // no password set
					DB:       0,  // use default DB
				}),
				MaxRecord: 100,
			},
			args: args{
				ctx:  context.Background(),
				name: "RUBEURasfs",
			},
			wantValues: nil,
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Client{
				Redis:     tt.fields.Redis,
				MaxRecord: tt.fields.MaxRecord,
			}
			gotValues, err := c.GetValueTransactions(tt.args.ctx, tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetValueTransactions() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotValues, tt.wantValues) {
				t.Errorf("GetValueTransactions() gotValues = %v, want %v", gotValues, tt.wantValues)
			}
		})
	}
}

func TestClient_GetListTransactions(t *testing.T) {
	type fields struct {
		Redis     *redis.Client
		MaxRecord int
	}
	type args struct {
		ctx context.Context
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		{
			name: "Test 1: Successful",
			fields: fields{
				Redis: redis.NewClient(&redis.Options{
					Addr:     "localhost:6379",
					Password: "", // no password set
					DB:       0,  // use default DB
				}),
				MaxRecord: 100,
			},
			args: args{
				ctx: context.Background(),
				key: "RUBJPY",
			},
			want:    []string{"RUBJPY|Aug 15 18:20:00"},
			wantErr: false,
		},
		{
			name: "Test 2: Unsuccessful",
			fields: fields{
				Redis: redis.NewClient(&redis.Options{
					Addr:     "localhost:6379",
					Password: "", // no password set
					DB:       0,  // use default DB
				}),
				MaxRecord: 100,
			},
			args: args{
				ctx: context.Background(),
				key: "RUBEUR"},
			want:    nil,
			wantErr: true,
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Client{
				Redis:     tt.fields.Redis,
				MaxRecord: tt.fields.MaxRecord,
			}
			got, err := c.GetListTransactions(tt.args.ctx, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetListTransactions() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetListTransactions() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestClient_GetOfNumberTransactions(t *testing.T) {
	type fields struct {
		Redis     *redis.Client
		MaxRecord int
	}
	type args struct {
		ctx  context.Context
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "Test 1: Successful",
			fields: fields{
				Redis: redis.NewClient(&redis.Options{
					Addr:     "localhost:6379",
					Password: "", // no password set
					DB:       0,  // use default DB
				}),
				MaxRecord: 100,
			},
			args: args{
				ctx:  context.Background(),
				name: "RUBJPY",
			},
			want:    1,
			wantErr: false,
		},
		{
			name: "Test 2: Unsuccessful",
			fields: fields{
				Redis: redis.NewClient(&redis.Options{
					Addr:     "localhost:6379",
					Password: "", // no password set
					DB:       0,  // use default DB
				}),
				MaxRecord: 100,
			},
			args: args{
				ctx:  context.Background(),
				name: "RUBEUR",
			},
			want:    0,
			wantErr: true,
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Client{
				Redis:     tt.fields.Redis,
				MaxRecord: tt.fields.MaxRecord,
			}
			got, err := c.GetOfNumberTransactions(tt.args.ctx, tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetOfNumberTransactions() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetOfNumberTransactions() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestClient_ResetOldRecord(t *testing.T) {
	type fields struct {
		Redis     *redis.Client
		MaxRecord int
	}
	type args struct {
		ctx  context.Context
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Test 1: Successful",
			fields: fields{
				Redis: redis.NewClient(&redis.Options{
					Addr:     "localhost:6379",
					Password: "", // no password set
					DB:       0,  // use default DB
				}),
				MaxRecord: 100,
			},
			args: args{
				ctx:  context.Background(),
				name: "RUBJPY",
			},
			wantErr: false,
		},
		{
			name: "Test 2: Unsuccessful",
			fields: fields{
				Redis: redis.NewClient(&redis.Options{
					Addr:     "localhost:6379",
					Password: "", // no password set
					DB:       0,  // use default DB
				}),
				MaxRecord: 100,
			},
			args: args{
				ctx:  context.Background(),
				name: "RUBEUR",
			},
			wantErr: true,
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Client{
				Redis:     tt.fields.Redis,
				MaxRecord: tt.fields.MaxRecord,
			}
			if err := c.ResetOldRecord(tt.args.ctx, tt.args.name); (err != nil) != tt.wantErr {
				t.Errorf("ResetOldRecord() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
