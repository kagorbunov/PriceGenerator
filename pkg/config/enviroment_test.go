package config

import (
	"reflect"
	"testing"
)

func Test_getEnv(t *testing.T) {
	type args struct {
		key        string
		defaultVal string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Test №1: Successful",
			args: args{
				"ADDR",
				"localhost:8080",
			},
			want: "localhost:8090",
		},
		{
			name: "Test №2: Unsuccessful",
			args: args{
				"ROUTER",
				"localhost:8080",
			},
			want: "localhost:8080",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getEnv(tt.args.key, tt.args.defaultVal); got != tt.want {
				t.Errorf("getEnv() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getEnvAsInt(t *testing.T) {
	type args struct {
		name       string
		defaultVal int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test №1: Successful",
			args: args{
				"MAX_RECORD",
				50,
			},
			want: 100,
		},
		{
			name: "Test №2: Unsuccessful",
			args: args{
				"ROUTER",
				100,
			},
			want: 100,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getEnvAsInt(tt.args.name, tt.args.defaultVal); got != tt.want {
				t.Errorf("getEnvAsInt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getEnvAsSlice(t *testing.T) {
	type args struct {
		name       string
		defaultVal []string
		sep        string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Test №1: Successful",
			args: args{
				"CURRENCY_PAIRS",
				[]string{"EURUSD", "USDRUB"},
				",",
			},
			want: []string{"EURUSD", "USDRUB", "USDJPY"},
		},
		{
			name: "Test №2: Unsuccessful",
			args: args{
				"TIMEFRAME_LOCAL",
				[]string{"EURUSD", "USDRUB", "USDJPY"},
				",",
			},
			want: []string{"EURUSD", "USDRUB", "USDJPY"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getEnvAsSlice(tt.args.name, tt.args.defaultVal, tt.args.sep); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getEnvAsSlice() = %v, want %v", got, tt.want)
			}
		})
	}
}
