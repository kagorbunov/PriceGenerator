package config

func New() *Config {
	return &Config{
		Redis: &RedisConfig{
			MaxRecord: getEnvAsInt("MAX_RECORD", 100),
			Addr:      getEnv("REDIS_ADDR", ""),
			Password:  getEnv("REDIS_PASSWORD", ""), // no password set
			DB:        getEnvAsInt("REDIS_DB", 0),   // use default DB
		},
		CurrencyPairs: getEnvAsSlice("CURRENCY_PAIRS", []string{"USDRUB"}, ","),
		Templates:     getEnvAsSlice("TEMPLATE_CURRENCY_PAIRS", []string{"70+5sin"}, ","),
		Addr:          getEnv("ADDR", "localhost:8090"),
	}
}

type Config struct {
	Redis         *RedisConfig `json:"redis"`
	CurrencyPairs []string     `json:"currency_pairs"`
	Templates     []string     `json:"templates"`
	Addr          string       `json:"addr"`
}

type RedisConfig struct {
	MaxRecord int    `json:"maxRecord"`
	Addr      string `json:"addr"`
	Password  string `json:"password"`
	DB        int    `json:"db"`
}
