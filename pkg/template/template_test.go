package template

import (
	"reflect"
	"testing"
	"time"
)

func TestCurrencyPairs_GenTransactionValues(t *testing.T) {
	type fields struct {
		CurrencyPairs []*CurrencyPair
	}
	type args struct {
		t time.Time
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		wantNames  []string
		wantValues []float64
	}{
		{
			name: "Test 1: Successful",
			fields: fields{
				CurrencyPairs: []*CurrencyPair{
					{
						Name:      "USDRUB",
						Midrange:  70,
						Amplitude: 5,
						Function:  "sin",
					},
					{
						Name:      "USDJPY",
						Midrange:  130,
						Amplitude: 10,
						Function:  "cos",
					},
				},
			},
			args: args{
				t: time.Date(0, 0, 0, 0, 0, 1, 0, time.UTC),
			},
			wantNames:  []string{"USDRUB", "USDJPY"},
			wantValues: []float64{74.20735492403948, 135.40302305868138},
		},
		{
			name: "Test 2: Unsuccessful",
			fields: fields{
				CurrencyPairs: []*CurrencyPair{
					{
						Name:      "USDRUB",
						Midrange:  70,
						Amplitude: 5,
						Function:  "sin",
					},
					{
						Name:      "USDJPY",
						Midrange:  130,
						Amplitude: 10,
						Function:  "",
					},
				},
			},
			args: args{
				t: time.Date(0, 0, 0, 0, 0, 1, 0, time.UTC),
			},
			wantNames:  []string{"USDRUB", "USDJPY"},
			wantValues: []float64{74.20735492403948, 140},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &CurrencyPairs{
				CurrencyPairs: tt.fields.CurrencyPairs,
			}
			gotNames, gotValues := c.GenTransactionValues(tt.args.t)
			if !reflect.DeepEqual(gotNames, tt.wantNames) {
				t.Errorf("GenTransactionValues() gotNames = %v, want %v", gotNames, tt.wantNames)
			}
			if !reflect.DeepEqual(gotValues, tt.wantValues) {
				t.Errorf("GenTransactionValues() gotValues = %v, want %v", gotValues, tt.wantValues)
			}
		})
	}
}

func TestNewCurrencyPair(t *testing.T) {
	type args struct {
		key   string
		value []string
	}
	tests := []struct {
		name string
		args args
		want *CurrencyPair
	}{
		{
			name: "Test 1: Successful",
			args: args{
				key:   "USDRUB",
				value: []string{"70", "5", "cos"},
			},
			want: &CurrencyPair{
				Name:      "USDRUB",
				Midrange:  70,
				Amplitude: 5,
				Function:  "cos",
			},
		},
		{
			name: "Test 2: Successful",
			args: args{
				key:   "USDJPY",
				value: []string{"130", "5", "sin"},
			},
			want: &CurrencyPair{
				Name:      "USDJPY",
				Midrange:  130,
				Amplitude: 5,
				Function:  "sin",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewCurrencyPair(tt.args.key, tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewCurrencyPair() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewCurrencyPairs(t *testing.T) {
	type args struct {
		listCP       []string
		listTemplate []string
	}
	tests := []struct {
		name string
		args args
		want *CurrencyPairs
	}{
		{
			name: "Test 1: Successful",
			args: args{
				listCP:       []string{"USDJPY"},
				listTemplate: []string{"130|5|cos"},
			},
			want: &CurrencyPairs{
				CurrencyPairs: []*CurrencyPair{
					{
						Name:      "USDJPY",
						Midrange:  130,
						Amplitude: 5,
						Function:  "cos",
					},
				},
			},
		},
		{
			name: "Test 2: Successful",
			args: args{
				listCP:       []string{"EURUSD", "USDRUB", "USDJPY"},
				listTemplate: []string{"1|0.1|cos", "70|5|sin", "130|5|cos"},
			},
			want: &CurrencyPairs{
				CurrencyPairs: []*CurrencyPair{
					{
						Name:      "EURUSD",
						Midrange:  1,
						Amplitude: 0.1,
						Function:  "cos",
					},
					{
						Name:      "USDRUB",
						Midrange:  70,
						Amplitude: 5,
						Function:  "sin",
					},
					{
						Name:      "USDJPY",
						Midrange:  130,
						Amplitude: 5,
						Function:  "cos",
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewCurrencyPairs(tt.args.listCP, tt.args.listTemplate); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewCurrencyPairs() = %v, want %v", got, tt.want)
			}
		})
	}
}
