package template

import (
	"log"
	"math"
	"strconv"
	"strings"
	"time"
)

type CurrencyPairs struct {
	CurrencyPairs []*CurrencyPair `json:"currency_pairs"`
}

type CurrencyPair struct {
	Name      string
	Midrange  float64
	Amplitude float64
	Function  string
}

func NewCurrencyPair(key string, value []string) *CurrencyPair {
	midrange, err := strconv.ParseFloat(value[0], 64)
	if err != nil {
		log.Fatalf("Error parsing template: %v", err)
	}
	amplitude, err := strconv.ParseFloat(value[1], 64)
	if err != nil {
		log.Fatalf("Error parsing template: %v", err)
	}
	return &CurrencyPair{Name: key, Midrange: midrange, Amplitude: amplitude, Function: value[2]}
}

func (c *CurrencyPairs) GenTransactionValues(t time.Time) (names []string, values []float64) {
	for _, cp := range c.CurrencyPairs {
		switch cp.Function {
		case "sin":
			values = append(values, cp.Midrange+cp.Amplitude*math.Sin(float64(t.Second())))
		case "cos":
			values = append(values, cp.Midrange+cp.Amplitude*math.Cos(float64(t.Second())))
		default:
			values = append(values, cp.Midrange+cp.Amplitude)
		}
		names = append(names, cp.Name)
	}
	return
}

func NewCurrencyPairs(listCP, listTemplate []string) *CurrencyPairs {
	var cp []*CurrencyPair
	for i, n := range listCP {
		cPair := NewCurrencyPair(n, strings.Split(listTemplate[i], "|"))
		cp = append(cp, cPair)
	}
	return &CurrencyPairs{CurrencyPairs: cp}
}
