package handler

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/kagorbunov/PriceGenerator/pkg/storage"
	"log"
	"net/http"
	"strings"
)

type Handler struct {
	ctx      context.Context
	Redis    *storage.Client
	Router   *mux.Router
	currency []string
}

func NewHandler(ctx context.Context, redis *storage.Client, Router *mux.Router, currency []string) *Handler {
	return &Handler{ctx: ctx, Redis: redis, Router: Router, currency: currency}
}
func (h *Handler) InitializeRoutes() {
	h.Router.HandleFunc("/currency_pair", h.GetAllCurrency).Methods("GET")
	h.Router.HandleFunc("/currency_pair/{cPair}", h.GetCurrency).Methods("GET")
}

func (h *Handler) GetAllCurrency(w http.ResponseWriter, r *http.Request) {
	resp := map[string][]string{
		"currency": h.currency,
	}
	respondWithJSON(w, http.StatusOK, resp)
}

func (h *Handler) GetCurrency(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cPair := vars["cPair"]
	cPairs, err := h.Redis.GetListTransactions(h.ctx, cPair)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
	}
	cValues, err := h.Redis.GetValueTransactions(h.ctx, cPair)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
	}
	var listTH []*storage.TransactionHistory
	for n, cData := range cPairs {
		cData = strings.Split(cData, "|")[1]
		th := storage.NewTransactionHistory(cData, cValues[n])
		listTH = append(listTH, th)
	}
	c := storage.NewCurrency(cPair, listTH)
	respondWithJSON(w, http.StatusOK, c)
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, err := w.Write(response)
	if err != nil {
		log.Fatalf("Error with response: %v", err)
	}
}
