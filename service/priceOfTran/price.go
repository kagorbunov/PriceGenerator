package priceOfTran

import (
	"context"
	"fmt"
	"gitlab.com/kagorbunov/PriceGenerator/pkg/storage"
	"gitlab.com/kagorbunov/PriceGenerator/pkg/template"
	"log"
	"time"
)

func GeneratePrice(ctx context.Context, listCP *template.CurrencyPairs, r *storage.Client) error {
	t := time.Now()
	listNames, listVal := listCP.GenTransactionValues(t)
	for n, name := range listNames {
		err := r.Set(ctx, name, fmt.Sprintf("%v", listVal[n]), t)
		if err != nil {
			log.Fatalf("Error: %v", err)
			return err
		}
		err = r.ResetOldRecord(ctx, name)
		if err != nil {
			log.Fatalf("Error: %v", err)
			return err
		}
	}
	time.Sleep(time.Second)
	return nil
}
