package priceOfTran

import (
	"context"
	"github.com/go-redis/redis/v9"
	"gitlab.com/kagorbunov/PriceGenerator/pkg/storage"
	"gitlab.com/kagorbunov/PriceGenerator/pkg/template"
	"testing"
)

func TestGeneratePrice(t *testing.T) {
	type args struct {
		ctx    context.Context
		listCP *template.CurrencyPairs
		r      *storage.Client
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Test 1: Successful",
			args: args{
				ctx: context.Background(),
				listCP: &template.CurrencyPairs{
					CurrencyPairs: []*template.CurrencyPair{
						{
							Name:      "USDRUB",
							Midrange:  70,
							Amplitude: 5,
							Function:  "sin",
						},
						{
							Name:      "USDJPY",
							Midrange:  130,
							Amplitude: 10,
							Function:  "cos",
						},
					},
				},
				r: &storage.Client{
					Redis: redis.NewClient(&redis.Options{
						Addr:     "localhost:6379",
						Password: "", // no password set
						DB:       0,  // use default DB
					}),
					MaxRecord: 100,
				},
			},
			wantErr: false,
		},
		{
			name: "Test 2: Successful",
			args: args{
				ctx: context.Background(),
				listCP: &template.CurrencyPairs{
					CurrencyPairs: []*template.CurrencyPair{
						{
							Name:      "USDRUB",
							Midrange:  70,
							Amplitude: 5,
							Function:  "sinc",
						},
						{
							Name:      "USDJPY",
							Midrange:  130,
							Amplitude: 10,
							Function:  "cos",
						},
					},
				},
				r: &storage.Client{
					Redis: redis.NewClient(&redis.Options{
						Addr:     "localhost:6379",
						Password: "", // no password set
						DB:       0,  // use default DB
					}),
					MaxRecord: 100,
				},
			},
			wantErr: false,
		},
		{
			name: "Test 3: Unsuccessful",
			args: args{
				ctx: context.Background(),
				listCP: &template.CurrencyPairs{
					CurrencyPairs: []*template.CurrencyPair{
						{
							Name:      "USDRUB",
							Midrange:  70,
							Amplitude: 5,
							Function:  "sin",
						},
						{
							Name:      "USDJPY",
							Midrange:  130,
							Amplitude: 10,
							Function:  "",
						},
					},
				},
				r: &storage.Client{
					Redis: redis.NewClient(&redis.Options{
						Addr:     "localhost:6379",
						Password: "", // no password set
						DB:       0,  // use default DB
					}),
					MaxRecord: 100,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := GeneratePrice(tt.args.ctx, tt.args.listCP, tt.args.r); (err != nil) != tt.wantErr {
				t.Errorf("GeneratePrice() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
