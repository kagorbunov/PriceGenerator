package main

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v9"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gitlab.com/kagorbunov/PriceGenerator/pkg/config"
	"gitlab.com/kagorbunov/PriceGenerator/pkg/handler"
	"gitlab.com/kagorbunov/PriceGenerator/pkg/storage"
	"gitlab.com/kagorbunov/PriceGenerator/pkg/template"
	"gitlab.com/kagorbunov/PriceGenerator/service/priceOfTran"
	"log"
	"net/http"
)

const appName = "service.PriceGenerator"

func main() {
	fmt.Println("Project Name: ", appName)

	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	cfg := config.New()

	cPairs := template.NewCurrencyPairs(cfg.CurrencyPairs, cfg.Templates)

	rdb := redis.NewClient(&redis.Options{
		Addr:     cfg.Redis.Addr,
		Password: cfg.Redis.Password, // no password set
		DB:       cfg.Redis.DB,       // use default DB
	})
	redis := storage.New(rdb, cfg.Redis.MaxRecord)
	go func(ctx context.Context, cPairs *template.CurrencyPairs, redis *storage.Client) {
		for {
			if err := priceOfTran.GeneratePrice(ctx, cPairs, redis); err != nil {
				log.Printf("Error generating price: %v", err)
			}
		}
	}(ctx, cPairs, redis)

	router := mux.NewRouter()
	h := handler.NewHandler(ctx, redis, router, cfg.CurrencyPairs)
	h.InitializeRoutes()
	log.Fatal(http.ListenAndServe(cfg.Addr, router))
}
