module gitlab.com/kagorbunov/PriceGenerator

go 1.17

require (
	github.com/go-redis/redis/v9 v9.0.0-beta.2
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0 // indirect
)
