FROM golang:1.17-buster

RUN go version
ENV GOPATH=/

COPY ./ ./

# build go app
RUN go mod download
RUN go build -o price-generator ./main.go

CMD ["./price-generator"]